#!/usr/bin/env python3
# -*- coding: utf-8 name> -*-

from datetime import date
from datetime import datetime
from re import DEBUG
from matplotlib import pyplot as plt
from os.path import abspath
from pathlib import Path
import argparse
import csv
import math
import numpy as np
import os
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA

plt.style.use('seaborn')
DEBUG = False
current_directory = Path(__file__).parent
spam_path = os.path.abspath(str(current_directory)+'/../data/spambase/spambase.data')

def is_spam(target): 
    """Returns true if last column is marked as spam."""
    return np.isclose(np.float64(1.0), np.float64(target)) 

def split_data_for_training(data):
    """
    Split data between training and test data.
    """
    np.random.shuffle(data)
    X = data[:,:-1]
    target = data[:,-1] 
    train_input, test_input, train_target, test_target = train_test_split(X, target, test_size=0.5, random_state=42)
    return (train_input, test_input, train_target, test_target) 

def probability_spam(training_target):
    """ 
    Net probability of spam for data.
    """
    spam_count = np.float64(sum([ 1 if is_spam(target) else 0 for target in training_target]))
    total_count = np.float64(len(training_target))
    return spam_count/total_count 

def probability_ham(training_target):
    """
    Net probability of non spam for data.
    """
    ham_count = np.float64(sum([ 1 if not is_spam(target) else 0 for target in training_target]))
    total_count = np.float64(len(training_target))
    return ham_count/total_count 

def data_distribution(data):
   """
   Compute the means and standard deviation of every feature in the dataset.
   """ 
   feature_means = np.mean(data, axis=0, dtype=np.float64)
   feature_stds  = [ max(std, 1e-4) for std in np.std(data, axis=0, dtype=np.float64)]
   return (feature_means, feature_stds)

def feature_log_likelihood_sum(input, data_distribution):
    """ 
    Compute the probability of feature given that the message is spam

    \\frac{1}{2\\pi\\sigma} e^{-\\frac{(x-\\mu)^2}}{2\\sigma^2}}
        - row norm.pdf(x, mean, std) 
    """
    means, stds = data_distribution
    sum = np.float64(0)
    for index, value in enumerate(input): 
        mean, std  = means[index], stds[index]
        p = norm(value, mean, std)
        if (np.isclose(p, 0.0)): p=1e-300
        sum+= np.log(p, dtype=np.float64)
    return sum 

def norm(x, mean, std): 
    """
    Compute the probability x
    """
    from scipy.stats import norm
    if std <= 0: std = 1e-4 
    g = norm.pdf(x,  mean, std)
    return g 

def spam_distributions(input, target): 
    data = []
    for index, value in enumerate(target):
        if np.isclose(value,1.0):
            data.append(input[index]) 
    spam_means, spam_stds = data_distribution(data)
    return (spam_means, spam_stds)

def ham_distributions(input, target):
    data = []
    for index, value in enumerate(target):
        if np.isclose(value,0.0):
            data.append(input[index]) 
    ham_means, ham_stds = data_distribution(data)
    return (ham_means, ham_stds)

def classify_all(training_input, test_input, training_target, test_target):
    spam_probability  = probability_spam(training_target)
    spam_distribution = spam_distributions(training_input, training_target)
    ham_probability   = probability_ham(training_target)
    ham_distribution  = ham_distributions(training_input, training_target)

    assert len(ham_distribution) == len(spam_distribution)

    spams,hams,errors = [], [], []
    for index, input in enumerate(test_input):
        target = test_target[index]
        classification = classify(spam_probability, spam_distribution, 
                                     ham_probability, ham_distribution, input, target) 
        if classification == 0:
            hams.append((input, target))
        elif classification == 1:
            spams.append((input, target))
        else :
            errors.append((input, target))
    return (spams, hams, errors)

def classify(spam_probability, spam_distribution, 
             ham_probability, ham_distribtion, input, target):
    """
    Read the features from the historical data.
    """
    feature_given_spam = feature_log_likelihood_sum(input, spam_distribution) 
    spam_sum = np.log(spam_probability) + feature_given_spam

    feature_given_ham    = feature_log_likelihood_sum(input, ham_distribtion) 
    ham_sum = np.log(ham_probability) + feature_given_ham 

    prediction = 0 if ham_sum > spam_sum else 1
    if DEBUG:
        if(np.isclose(prediction,target)):
            print("INCORRECT_PREDICTION", prediction,target, ham_sum, spam_sum, str(abs(ham_sum - spam_sum )))
        else:
            print("CORRECT_PREDICTION", prediction,target, ham_sum, spam_sum, str(abs(ham_sum - spam_sum )))

    return prediction

def read_data():
    """
    Read some of the spam data and print out a row count.
    """
    data = np.loadtxt(spam_path, delimiter=',', dtype=np.float64)
    return data

if __name__ == '__main__':
    """
    Run experiments by name, for example:
        ./main.py 
    """
    data = read_data()
    training, test, training_target, test_target  = split_data_for_training(data)
    spams, hams, errors = classify_all(training, test, training_target, test_target) 
    assert len(errors) == 0
    spam_correct, spam_incorrect = 0, 0
    for (spam, target) in spams: 
        if target == 1:
            spam_correct+=1
        else:
            spam_incorrect+=1 

    ham_correct, ham_incorrect = 0, 0
    for (ham, target) in hams: 
        if target == 0:
            ham_correct+=1
        else:
            ham_incorrect+=1 

    confusion_matrix = [[spam_correct, spam_incorrect], [ham_incorrect, ham_correct]]

    total_accuracy = (spam_correct+ham_correct)/(spam_correct+spam_incorrect+ham_correct+ham_incorrect)
    print("ACCURACY: ", total_accuracy)

    recall = (spam_correct)/(spam_correct+ham_incorrect)
    print("RECALL:", recall)

    precision=(spam_correct)/(spam_correct+spam_incorrect)
    print("PRECISION:", precision)

    print("CONFUSION_MATRIX", confusion_matrix)

    print("PCA Singular Values:")
    import termtables as tt
    pca = PCA(n_components=57)
    pca.fit(test)
    print("PCA Explained Variance Ratio", pca.explained_variance_ratio_)
    print(pca.singular_values_)