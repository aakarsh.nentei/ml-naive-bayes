import numpy as np
from src.main import  classify, classify_all, data_distribution, feature_log_likelihood_sum, ham_distributions, probability_ham, probability_spam, read_data, spam_distributions, split_data_for_training
import math

def test_split_data_for_training():
    """
    Compute the data probablity
    """
    probability  = probability_spam([1, 0,0])
    assert probability <= (1/3)
    assert probability_ham([1,0,0]) >= (2/3)


def test_probability_of_data():
    """
    Assert that all the probabilities are within acceptable ranges.
    """
    data = read_data() 
    training_input, test_input , training_target, test_target= split_data_for_training(data)

    assert probability_ham(training_target) >= probability_spam(training_target) 
    assert probability_ham(test_target) >= probability_spam(test_target) 
 
def test_data_distribution():
    data = read_data() 
    training_input, test_input , training_target, test_target = split_data_for_training(data)
    means, stds = data_distribution(training_input)
    assert len(means) == len(stds)
    assert len(means) == 57

def test_liklihoods_computation():
    data = read_data()
    training_input, test_input , training_target, test_target = split_data_for_training(data)
    spam_probability  = probability_spam(training_target)
    spam_distribution = spam_distributions(training_input, training_target)
    for row in test_input:
        feature_given_spam = feature_log_likelihood_sum(row, spam_distribution) 
        # print("ROW", row)        
        # print("FEATURE_GIVEN_SPAM", feature_given_spam)        

    # print("SPAM_DISTRIBUTION", spam_distribution)
"""
def test_spam_distrbution_means():
    data = read_data() 
    training_input, test_input , training_target, test_target = split_data_for_training(data)
    spam_distribution = spam_distributions(training_input, training_target)
    spam_means,spam_stds = spam_distribution
    total_count = len(list(filter(lambda t: int(t[-1]) == 1, training)))
    totals = [0 for i in range(total_count)]
    for row in training: 
        if row[-1] == 1:
            for i in range(len(row[:-1])):
                totals[i]+=row[i]
    nfeatures = len(training[0][:-1])
    for i in range(nfeatures):
        assert np.isclose(totals[i]/total_count, spam_means[i])


def test_ham_distrbution_means():
    data = read_data() 
    training, test = split_data_for_training(data)
    ham_distribution = ham_distributions(training)
    ham_means,ham_stds = ham_distribution
    total_count = len(list(filter(lambda t: int(t[-1]) == 0, training)))
    totals = [0 for i in range(total_count)]
    for row in training: 
        if row[-1] == 0:
            for i in range(len(row[:-1])):
                totals[i]+=row[i]
    nfeatures = len(training[0][:-1])
    for i in range(nfeatures):
        assert np.isclose(totals[i]/total_count, ham_means[i])
 
def test_ham_distrbution_std():
    data = read_data() 
    training, test = split_data_for_training(data)
    ham_distribution = ham_distributions(training)
    ham_means,ham_stds = ham_distribution
    total_count = len(list(filter(lambda t: int(t[-1]) == 0, training)))
    totals = [0 for i in range(total_count)]
    square_totals = [ 0 for i in range(total_count) ]
    for row in training: 
        if row[-1] == 0:
            for i in range(len(row[:-1])):
                totals[i]+=row[i]
                square_totals[i]+= row[i]*row[i]
    nfeatures = len(training[0][:-1])
    for i in range(nfeatures):
        std = math.sqrt((total_count*square_totals[i] - totals[i]**2)/(total_count**2))
        assert np.isclose(std, ham_stds[i], rtol=0.001, atol=0.0001)

def test_spam_distrbution_std():
    data = read_data() 
    training, test = split_data_for_training(data)
    spam_distribution = spam_distributions(training)
    spam_means,spam_stds = spam_distribution
    total_count = len(list(filter(lambda t: int(t[-1]) == 1, training)))
    totals = [0 for i in range(total_count)]
    square_totals = [ 0 for i in range(total_count) ]
    for row in training: 
        if row[-1] == 1:
            for i in range(len(row[:-1])):
                totals[i]+=row[i]
                square_totals[i]+= row[i]*row[i]
    nfeatures = len(training[0][:-1])
    for i in range(nfeatures):
        std = math.sqrt((total_count*square_totals[i] - totals[i]**2)/(total_count**2))
        assert np.isclose(std, spam_stds[i], rtol=0.0001, atol=0.0001)
 
"""
def test_classification_accuracy():
    data = read_data()
    training, test, training_target, test_target  = split_data_for_training(data)
    spams, hams, errors = classify_all(training, test, training_target, test_target) 
    assert len(errors) == 0
    spam_correct, spam_incorrect = 0, 0
    for (spam, target) in spams: 
        if target == 1:
            spam_correct+=1
        else:
            spam_incorrect+=1 

    print("spam_correct", spam_correct, "spam_incorrect", spam_incorrect)
    ham_correct, ham_incorrect = 0, 0
    for (ham, target) in hams: 
        if target == 0:
            ham_correct+=1
        else:
            ham_incorrect+=1 

    print("ham_correct", ham_correct, "ham_incorrect", ham_incorrect)

    total_accuracy = (spam_correct+ham_correct)/(spam_correct+spam_incorrect+ham_correct+ham_incorrect)
    print("total accuracy", total_accuracy)
    print("TEST", len(test))

    # incorrect, correct = 0,0

    """
    for x in test:
        predicted_classification =  classify(training, x)
        if predicted_classification == x[-1]:
            correct+=1
        else:
            incorrect+=1
    accuracy = correct/(incorrect+correct)
    # print("ACCURACY", accuracy)
    """ 